Informaci�n �til:

- Bootstrap: si el bootstrap original no coincide con lo que se quer�a, se pueden editar las variables r�pidamente en bootstrap/_variables.scss


Imports de main.scss

- Reset: se importa el reset de Eric Meyer al comienzo del archivo main.scss
- Mixins: hay una colecci�n de mixins que facilita la escritura de c�digo cross-browser (los que usan prefijo -moz, -webkit, etc). add_ons/_css3-mixins.scss. El archivo add_ons/css3-mixins - Available mixins documenta los mixins disponibles y los par�metros que usan.


- Variables del sitio: se recomienda guardar colores, fuentes y tama�os que se van a utilizar repetidamente en general/_variables.scss
- Header y footer: general/_header.scss y general/_footer.scss
- Elementos gen�ricos: general/_generic.scss
- Elementos de cada seccion: sections/_section_name.scss - Estos se tienen que incluir manualmente, segun el sitio.


Otros elementos �tiles: 
- Social icons. Font armada en Fontello, con �conos de redes sociales en distintos formatos. Se tiene que incluir el css, y luego se puede utilizar de modo parecido a los glyphicons de Boostrap. 



-----

Software gratuito para procesar SCSS: prepros